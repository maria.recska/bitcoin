import React from 'react'

export default function Coin({symbol, buy, sell, last}) {
    return (
        <div className="coin-cointainer">
           <div className="coin-row">
               <div className="coin">
                   <p className="coin-symbol">{symbol}</p>
               </div>
               <div className="coin-data">
                   <p className="coin-buy">${buy}</p>
                   <p className="coin-sell">${sell}</p>
                   <p clasName="coin-last">${last}</p>
               </div>
           </div>
        </div>
    )
}
