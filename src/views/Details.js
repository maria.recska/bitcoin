import React, { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import DetailsTable from "../components/DetailsTable";
import BitcoinNavbar from "../components/Navbar";
import getDifficulty from "../services/getDifficulty";
import getHashRate from "../services/getHashRate";
import getTotalBc from "../services/getTotalBc";
import get24HrBtcSent from "../services/get24HrBtcSent";
import getMarketCap from "../services/getMarketCap";
import get24HrTransaction from "../services/get24HrTransaction";

export default function Details() {
  const [difficulty, setDifficulty] = useState(null);
  const [hashrate, setHashrate] = useState(null);
  const [totalBc, setTotalBc] = useState(null);
  const [btcSent, setBtcSent] = useState(null);
  const [marketCap, setMarketCap] = useState(null);
  const [trans, getTrans] = useState(null);

  useEffect(() => {
    getDifficulty().then((diffData) => {
      setDifficulty(() => diffData);
    });
    getHashRate().then((hashData) => {
      setHashrate(() => hashData);
    });
    getTotalBc().then((bcData) => {
      setTotalBc(() => bcData);
    });
    get24HrBtcSent().then((bcSentData) => {
      setBtcSent(() => bcSentData);
    });
    getMarketCap().then((marketCapData) => {
      setMarketCap(() => marketCapData);
    });
    get24HrTransaction().then((Trans24Data) => {
      getTrans(() => Trans24Data);
    });
  }, []);

  return (
    <div>
      <BitcoinNavbar />
      <Container>
        <DetailsTable
          difficulty={difficulty}
          hashrate={hashrate}
          totalBc={totalBc}
          btcSent={btcSent}
          marketCap={marketCap}
          trans={trans}
        />
      </Container>
    </div>
  );
}
