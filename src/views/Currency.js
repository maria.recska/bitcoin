import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import getBitcoin from "../services/getBitcoin";
import CurrencyTable from "../components/CurrencyTable";
import BitcoinNavbar from "../components/Navbar";

function Currency() {
  const [currencyOptions, setCurrencyOptions] = useState({ currency: {} });

  useEffect(() => {
    getBitcoin().then((data) => {
      setCurrencyOptions((prevCurrencyOptions) => ({
        ...data,
      }));
    });
  }, []);

  return (
    <div>
      <BitcoinNavbar />
      <Container>
        <div>
          <CurrencyTable currencyOptions={currencyOptions} />
        </div>
      </Container>
    </div>
  );
}

export default Currency;
