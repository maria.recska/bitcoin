import React from "react";
import Currency from "./views/Currency";
import Details from "./views/Details";
import Calculator from "./views/Calculator";
import Chart from "./views/Chart";
import MyBitcoin from "./views/MyBitcoin";
import {
  HashRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import './App.css';


function App() {

  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/currency"><Currency/></Route>
          <Route path="/details"><Details/></Route>
          <Route path="/calculator"><Calculator/></Route>
          <Route path="/chart"><Chart/></Route>
          <Route path="/mybitcoin"><MyBitcoin/></Route>
          <Route path="*"><Redirect to="/currency" /></Route>
      </Switch>
      </Router>
      </div>
  );
}

export default App;
