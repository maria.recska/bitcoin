import React from "react";
import "../style/style.css";

function DetailsTable ({ difficulty, hashrate, totalBc, btcSent, marketCap,  trans  }) {

  return (
    <div className="currency-container">
      <h2 className="currency-header">Actual Bitcoin Prices</h2>
      <table class="currency-table">
        <thead className="currency-title">
        <th>MARKET CAPITALISATION</th>
        <th>CURRENT DIFFICULTY TARGET</th>
        <th>TOTAL BITCOINS IN CIRCULATION</th>
        <th>ESTIMATED NETWORK HASH RATE</th>
        <th>NUMBER OF TRANSACTIONS IN THE PAST 24 HOURS</th>
        <th>NUMBER OF BITCOINS SENT IN THE PAST 24 HOURS</th>
        </thead>
        <tbody>
          <tr>
            <td>{marketCap}</td>
            <td>{difficulty}</td>
            <td>{totalBc}</td>
            <td>{hashrate}</td>
            <td>{trans}</td>
            <td>{btcSent}</td>
          </tr>
            
      </tbody>
    </table>
    </div>
  );
};

export default DetailsTable;
