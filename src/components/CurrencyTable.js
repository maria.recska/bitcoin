import React from "react";
import "../style/style.css";

function CurrencyTable ({ currencyOptions }) {

  return (
    <div className="currency-container">
      <h2 className="currency-header">Actual Bitcoin Prices</h2>
      <table class="currency-table">
        <thead className="currency-title">
        <th>CURRENCY</th>
        <th>SYMBOL</th>
        <th>SELL</th>
        <th>BUY</th>
        </thead>
        <tbody>
      {Object.entries(currencyOptions).map(([key, currency]) =>(
        <tr key={key}>
        <td>{key}</td>
        <td>{currency.symbol}</td>
        <td>{currency.sell}</td>
        <td>{currency.buy}</td>
      </tr>
      ))}
      </tbody>
    </table>
    </div>
  );
};

export default CurrencyTable;
