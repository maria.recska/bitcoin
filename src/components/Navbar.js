import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import React from "react";

export default function BitcoinNavbar() {
  return (
    <div>
      <Navbar bg="dark" variant="dark" style={{width: "100%, margin: 0", justifyContent: "center"}}>
        <Navbar.Brand href="#currency">Actual Bitcoin Prices</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link href="#details">Bitcoin Details</Nav.Link>
          <Nav.Link href="#calculator">Bitcoin Calculator</Nav.Link>
          <Nav.Link href="#chart">Bitcoin Chart</Nav.Link>
          <Nav.Link href="#mybitcoin">My Bitcoin</Nav.Link>
        </Nav>
      </Navbar>
    </div>
  );
}
