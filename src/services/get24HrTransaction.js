import axios from "axios";

async function get24HrTransaction() {
  try {
    const data = await axios.get('https://blockchain.info/q/24hrtransactioncount');
    
    const Trans24Data = data.data;

    return Trans24Data;
   
  } catch(error) {
    console.log("error", error);
  }
}

export default get24HrTransaction;
