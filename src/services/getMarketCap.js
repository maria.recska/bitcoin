import axios from "axios";

async function getMarketCap() {
  try {
    const data = await axios.get('https://blockchain.info/q/marketcap');

    const marketCapData = data.data;

    return marketCapData;
   
  } catch(error) {
    console.log("error", error);
  }
}

export default getMarketCap;
