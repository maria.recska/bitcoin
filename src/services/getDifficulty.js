import axios from "axios";
//for some reason it did not work with fetch, so I switched to axios. 

async function getDifficulty() {
  try {
    const data = await axios.get("https://blockchain.info/q/getdifficulty");
    
    const diffData = data.data;

    return diffData;
    
  } catch(error) {
    console.log("error", error);
  }
}

export default getDifficulty;
