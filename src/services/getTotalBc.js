import axios from "axios";

async function getTotalBc() {
  try {
    const data = await axios.get('https://blockchain.info/q/totalbc');
    
    const bcData = data.data;
    
    return bcData;
   
  } catch(error) {
    console.log("error", error);
  }
}

export default getTotalBc;
