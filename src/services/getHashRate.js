import axios from "axios";

async function getHashRate() {
  try {
    const data = await axios.get('https://blockchain.info/q/hashrate');
    
    const hashData = data.data;
    
    return hashData;
    
  } catch(error) {
    console.log("error", error);
  }
}

export default getHashRate;
