import axios from "axios";

async function get24HrBtcSent() {
  try {
    const data = await axios.get('https://blockchain.info/q/24hrbtcsent');
    
    const bcSentData = data.data;

    return bcSentData;
   
  } catch(error) {
    console.log("error", error);
  }
}

export default get24HrBtcSent;
